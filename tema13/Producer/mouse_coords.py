from pynput.mouse import Controller
from kafka import KafkaProducer
import threading
import time


class Producer(threading.Thread):
    def __init__(self, topic):
        super().__init__()
        self.topic = topic

    def run(self) -> None:
        producer = KafkaProducer()
        for _ in range(100):
            message = '{} {}'.format(mouse.position[0], mouse.position[1])
            # thread-ul producator trimite mesaje catre un topic
            producer.send(topic=self.topic, value=bytearray(message, encoding="utf-8"))
            print("Am produs mesajul: {}".format(message))
            time.sleep(0.1)

        # metoda flush() asigura trimiterea batch-ului de mesaje produse
        producer.flush()


mouse = Controller()

if __name__ == "__main__":
    producer_thread = Producer("mouse_coords")
    producer_thread.start()
    producer_thread.join()