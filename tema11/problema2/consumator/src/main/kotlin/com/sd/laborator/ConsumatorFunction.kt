package com.sd.laborator;

import io.micronaut.function.executor.FunctionInitializer
import io.micronaut.function.FunctionBean;
import org.w3c.dom.Document
import org.xml.sax.InputSource
import java.io.StringReader
import java.util.function.Consumer
import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory

@FunctionBean("consumator")
class ConsumatorFunction : FunctionInitializer(), Consumer<Consumator> {

    override fun accept(t: Consumator)
    {
        val doc: Document? = convertStringToXMLDocument(t.xml)
        var listaResult = getListTitleNHref(doc!!)

        //In loc de returnarea la iesirea standard, le-am afisat direct deoarece Consumer nu returneaza nimic
        for (pair in listaResult)
        {
            println("Title="+pair.first+", href="+pair.second)
        }

    }
    private fun convertStringToXMLDocument(xmlString: String): Document? { //Parser that produces DOM object trees from XML content
        val factory = DocumentBuilderFactory.newInstance()
        //API to obtain DOM Document instance
        var builder: DocumentBuilder? = null
        try { //Create DocumentBuilder with default configuration
            builder = factory.newDocumentBuilder()
            //Parse the content to Document object
            return builder.parse(InputSource(StringReader(xmlString)))
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }

    private fun getListTitleNHref(document: Document):List<Pair<String, String>>
    {
        var titles = document.getElementsByTagName("title")
        var links = document.getElementsByTagName("link")
        var result: MutableList<Pair<String, String>> = ArrayList()
        if(links.length!=titles.length)
            return result
        for (i in 0..titles.length-1)
        {
            var href =  links.item(i).attributes.item(0).textContent
            result.add(Pair(titles.item(i).textContent,href))

        }
        return result
    }
}

/**
 * This main method allows running the function as a CLI application using: echo '{}' | java -jar function.jar 
 * where the argument to echo is the JSON to be parsed.
 */
fun main(args : Array<String>) { 
    val function = ConsumatorFunction()
    function.run(args, { context -> function.accept(context.get(Consumator::class.java))})
}