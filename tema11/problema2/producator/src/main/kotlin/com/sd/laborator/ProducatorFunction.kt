package com.sd.laborator;

import io.micronaut.function.FunctionBean
import org.json.JSONObject
import java.util.function.Supplier
import khttp.get as httpGet


@FunctionBean("producator")
class ProducatorFunction : Supplier<String> {

    override fun get(): String {
        val r = httpGet("https://xkcd.com/atom.xml")

        return JSONObject().put("xml", r.text).toString()
    }

}

/**
 * This main method allows running the function as a CLI application using: echo '{}' | java -jar function.jar 
 * where the argument to echo is the JSON to be parsed.
 */
fun main(args : Array<String>) { 
    val function = ProducatorFunction()
    function.get()
}