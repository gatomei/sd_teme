package com.sd.laborator
import io.micronaut.core.annotation.Introspected

@Introspected
class EratosteneRequest {
	private lateinit var number: Integer
	private lateinit var lista: List<Integer>

	fun getNumber(): Int {
		return number.toInt()
	}
	fun getLista(): List<Int> {
		return lista!!.map{it.toInt()}
	}
}