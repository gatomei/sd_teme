class Bidder private constructor(val myName:String, val email:String, val phone: String){
    companion object {
        fun createBidder(name: String, email: String, phone: String): Bidder {
            return Bidder(name, email, phone)
        }
    }

    override fun toString(): String {
        return "Nume:${myName},Email:${email},Telefon:${phone}"
    }
}