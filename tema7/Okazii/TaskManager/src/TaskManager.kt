import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.subscribeBy
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.net.ServerSocket
import java.net.SocketTimeoutException

class TaskManagerMicroservice {
    private var taskManagerSocket: ServerSocket
    private var receiveTasksObservable: Observable<String>

    companion object Constants {
        const val TASK_MANAGER_PORT = 1400
        const val FILE_NAME = "logs.txt"
        const val TASK_DURATION: Long = 25_000
    }

    init {
        taskManagerSocket = ServerSocket(TASK_MANAGER_PORT)
        taskManagerSocket.setSoTimeout(TASK_DURATION.toInt())
        println("TaskManagerMicroservice se executa pe portul: ${taskManagerSocket.localPort}")
        File(FILE_NAME).writeText("Task Manager is running\n\n")
        val time = "Time".padEnd(32, ' ')
        val name = "Name".padEnd(30,' ')
        val status = "Status".padEnd(10,' ')
        val port = "Port".padEnd(10,' ')

        File(FILE_NAME).appendText("${time}\t\t${name}\t\t\t${status}\t\t\t${port}\n")
        // se creeaza obiectul Observable cu care se genereaza evenimente cand se primesc oferte de la servicii
        receiveTasksObservable = Observable.create<String> { emitter ->
            // se asteapta conexiuni din partea serviciilor
            while (true) {
                try {
                    val serviceConnection = taskManagerSocket.accept()

                    val bufferReader = BufferedReader(InputStreamReader(serviceConnection.inputStream))
                    val receivedMessage = bufferReader.readLine()

                    // daca se primeste un mesaj gol (NULL), atunci inseamna ca cealalta parte a socket-ului a fost inchisa

                        bufferReader.close()
                        serviceConnection.close()
                    // se emite ce s-a citit ca si element in fluxul de mesaje
                    if (receivedMessage != null)  {
                        emitter.onNext(receivedMessage)
                    }
                } catch (e: SocketTimeoutException) {
                    emitter.onComplete()
                    break
                }
            }
        }
    }

    private fun receiveTasks() {
        // se incepe prin a primi ofertele de la bidderi
        receiveTasksObservable.subscribeBy(
            onNext = {
                val message = Message.deserialize(it.toByteArray())
                println(message)
                val name = message.sender.split('-')[0]
                val port = message.sender.split('-')[1]
                File(FILE_NAME).appendText("[${message.timestamp}]\t\t\t${name.padEnd(30,' ')}\t\t\t${message.body.padEnd(10, ' ')}\t\t\t${port.padEnd(10,' ')}\n")
                //write in log file
            },
            onComplete = {
                println("Task Manager se inchide")
                File(FILE_NAME).appendText("\nTask Manager stopped running\n")

            },
            onError = { println("Eroare: $it") }
        )
    }



    fun run() {
        receiveTasks()
    }
}

fun main(args: Array<String>) {
    val taskManagerMicroservice = TaskManagerMicroservice()
    taskManagerMicroservice.run()
}