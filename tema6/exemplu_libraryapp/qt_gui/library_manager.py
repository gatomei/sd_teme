import os
import sys
import requests
import qdarkstyle
import json
from requests.exceptions import HTTPError
from PyQt5.QtWidgets import QWidget, QApplication, QFileDialog, QMessageBox, QLabel, QPushButton, QLineEdit, QTextEdit
from PyQt5 import QtCore
from PyQt5.uic import loadUi


def debug_trace(ui=None):
    from pdb import set_trace
    QtCore.pyqtRemoveInputHook()
    set_trace()
    # QtCore.pyqtRestoreInputHook()


class AddBookWidget(QWidget ):
    def __init__(self,LibraryApp):
        super().__init__()
        self.back=LibraryApp
        self.setWindowTitle("Adaugare carte")
        self.name_lbl= QLabel("Nume:", self)
        self.name_lbl.move(20,20)
        self.name_txt=QLineEdit(self)
        self.name_txt.move(80,20)
        self.autor_lbl= QLabel("Autor:", self)
        self.autor_lbl.move(20,50)
        self.autor_txt=QLineEdit(self)
        self.autor_txt.move(80,50)
        self.edit_lbl= QLabel("Editura:", self)
        self.edit_lbl.move(20,80)
        self.edit_txt=QLineEdit(self)
        self.edit_txt.move(80,80)
        self.cont_lbl=QLabel("Continut:", self)
        self.cont_lbl.move(20,110)
        self.cont_txt=QTextEdit(self)
        self.cont_txt.resize(200,200)
        self.cont_txt.move(80,110)
        self.nf_Add_btn= QPushButton("Adaugare", self)
        self.nf_Add_btn.clicked.connect(self.on_add_book)
        self.nf_Add_btn.move(285,200)
        self.show()

    def on_add_book(self):
        name=self.name_txt.text()
        autor=self.autor_txt.text()
        editura=self.edit_txt.text()
        continut=self.cont_txt.toPlainText()
        self.back.on_add_book(name, autor, editura, continut )
        self.hide()


class LibraryApp(QWidget):
    ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

    def __init__(self):
        super(LibraryApp, self).__init__()
        ui_path = os.path.join(self.ROOT_DIR, 'library_manager.ui')
        loadUi(ui_path, self)
        self.search_btn.clicked.connect(self.search)
        self.save_as_file_btn.clicked.connect(self.save_as_file)
        self.addBook_btn.clicked.connect(self.add_book)

    def search(self):
        search_string = self.search_bar.text()
        url = None

        if not search_string:
            if self.json_rb.isChecked():
                url = '/print?format=json'
            elif self.html_rb.isChecked():
                url = '/print?format=html'
            else:
                url = '/print?format=raw'
            
        else :
            if self.author_rb.isChecked():
                url = '/find?author={}'.format(search_string.replace(' ', '%20'))
            elif self.title_rb.isChecked():
                url = '/find?title={}'.format(search_string.replace(' ', '%20'))
            else:
                url = '/find?publisher={}'.format(search_string.replace(' ', '%20'))
        full_url = "http://localhost:8081" + url 
        try:
            response = requests.get(full_url)
            self.result.setText(response.content.decode('utf-8'))
        except HTTPError as http_err:
            print('HTTP error occurred: {}'.format(http_err))
        except Exception as err:
            print('Other error occurred: {}'.format(err))

    def save_as_file(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        file_path = str(
            QFileDialog.getSaveFileName(self,
                                        'Salvare fisier',
                                        options=options))
        if file_path:
            file_path = file_path.split("'")[1]
            if not file_path.endswith('.json') and not file_path.endswith(
                    '.html') and not file_path.endswith('.txt'):
                if self.json_rb.isChecked():
                    file_path += '.json'
                elif self.html_rb.isChecked():
                    file_path += '.html'
                else:
                    file_path += '.txt'
            try:
                with open(file_path, 'w') as fp:
                    if file_path.endswith(".html"):
                        fp.write(self.result.toHtml())
                    else:
                        fp.write(self.result.toPlainText())
            except Exception as e:
                print(e)
                QMessageBox.warning(self, 'Library Manager',
                                    'Nu s-a putut salva fisierul')


    def add_book(self):
        self.new_frame=AddBookWidget(self)

    def on_add_book(self, name, autor, editura, continut):
        self.new_frame.close()
        name="{}".format(name.replace(' ', '%20'))
        autor="{}".format(autor.replace(' ', '%20'))
        editura="{}".format(editura.replace(' ', '%20'))
        continut="{}".format(continut.replace(' ', '%20'))
        full_url = "http://localhost:8081/add"
        headers = {'content-type': 'application/json'}
        data_dict = {"title":name, "author":autor, "publisher":editura, "content":continut}
        requests.post(full_url, data=json.dumps(data_dict), headers=headers)
        

if __name__ == '__main__':
    app = QApplication(sys.argv)

    stylesheet = qdarkstyle.load_stylesheet_pyqt5()
    app.setStyleSheet(stylesheet)

    window = LibraryApp()
    window.show()
    sys.exit(app.exec_())
