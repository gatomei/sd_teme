package com.sd.laborator.services

import com.sd.laborator.interfaces.LibraryPrinter
import com.sd.laborator.model.Book
import org.springframework.stereotype.Service

@Service
class LibraryPrinterService : LibraryPrinter {
    override fun printHTML(books: Set<Book?>): String {
        var content: String = "<html><head><title>Libraria mea HTML</title></head><body>"
        books.forEach {
            content += "<p><h3>${it!!.Title}</h3><h4>${it!!.Author}</h4><h5>${it!!.Publisher}</h5>${it!!.Content}</p><br/>"
        }
        content += "</body></html>"
        return content
    }

    override fun printJSON(books: Set<Book?>): String {
        var content: String = "[\n"
        books.forEach {
            if (it != books.last())
                content += "    {\"Titlu\": \"${it!!.Title}\", \"Autor\":\"${it!!.Author}\", \"Editura\":\"${it!!.Publisher}\", \"Text\":\"${it!!.Content}\"},\n"
            else
                content += "    {\"Titlu\": \"${it!!.Title}\", \"Autor\":\"${it!!.Author}\", \"Editura\":\"${it!!.Publisher}\", \"Text\":\"${it!!.Content}\"}\n"
        }
        content += "]\n"
        return content
    }

    override fun printRaw(books: Set<Book?>): String {
        var content: String = ""
        books.forEach {
            content += "${it!!.Title}\n${it!!.Author}\n${it!!.Publisher}\n${it!!.Content}\n\n"
        }
        return content
    }
}