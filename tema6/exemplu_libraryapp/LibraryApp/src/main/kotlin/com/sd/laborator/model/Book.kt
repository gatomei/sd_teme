package com.sd.laborator.model

class Book(private var title: String, private var author: String,
           private var publisher: String, private var content: String) {

    var Title: String
        get() {
            return title
        }
        set(value) {
            title = value
        }

    var Author: String
        get() {
            return author
        }
        set(value) {
            author = value
        }

    var Publisher: String
        get() {
            return publisher
        }
        set(value) {
            publisher = value
        }

    var Content: String
        get() {
            return content
        }
        set(value) {
            content = value
        }


    fun hasAuthor(author: String): Boolean {
        return Author.equals(author)
    }

    fun hasTitle(title: String): Boolean {
        return Title.equals(title)
    }

    fun publishedBy(publisher: String): Boolean {
        return Publisher.equals(publisher)
    }

    override fun toString(): String {
        return "Title:"+title+", Author:"+author+", Publisher:"+publisher+", Content:"+content
    }
}