package com.sd.laborator.services

import com.sd.laborator.interfaces.LibraryDAO
import com.sd.laborator.model.Book
import com.sd.laborator.model.Content
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Service
import java.sql.ResultSet
import java.sql.SQLException

class BookRowMapper : RowMapper<Book?> {
    @Throws(SQLException::class)
    override fun mapRow(rs: ResultSet, rowNum: Int): Book {
        return Book(rs.getString("title"), rs.getString("author"),
                rs.getString("publisher"),
                rs.getString("content"))
    }
}

@Service
class LibraryDAOService : LibraryDAO {
    @Autowired
    private lateinit var jdbcTemplate: JdbcTemplate

    override fun createBookTable() {
        jdbcTemplate.execute(
                "CREATE TABLE IF NOT EXISTS books( id INTEGER PRIMARY KEY AUTOINCREMENT,author VARCHAR(100), title VARCHAR(100) UNIQUE, publisher VARCHAR(100) , content TEXT(100))")
    }

    override fun getBooks(): Set<Book?> {
        createBookTable()

        val result: MutableList<Book?> = jdbcTemplate.query("SELECT * FROM books", BookRowMapper())
        var stringResult: String = ""
        for (item in result) {
            stringResult += item
        }
        return result.toSet()
    }

    override fun addBook(book: Book) {
        jdbcTemplate.update("INSERT INTO books(title, author, publisher, content) " +
                "VALUES (?, ?, ?, ?)", book.Title, book.Author, book.Publisher, book.Content)
    }

    override fun findAllByAuthor(author: String): Set<Book?> {

        val result: MutableList<Book?> = jdbcTemplate.query("SELECT * FROM books b where b.author like '" + author + "';", BookRowMapper())
        var stringResult: String = ""
        for (item in result) {
            stringResult += item
        }
        return result.toSet()
    }

    override fun findAllByTitle(title: String): Set<Book?> {

        val result: MutableList<Book?> = jdbcTemplate.query("SELECT * FROM books b where b.title like '" + title + "';", BookRowMapper())
        var stringResult: String = ""
        for (item in result) {
            stringResult += item
        }
        return result.toSet()
    }

    override fun findAllByPublisher(publisher: String): Set<Book?> {
        val result: MutableList<Book?> = jdbcTemplate.query("SELECT * FROM books b where b.publisher like '" + publisher + "';", BookRowMapper())
        var stringResult: String = ""
        for (item in result) {
            stringResult += item
        }
        return result.toSet()
    }
}