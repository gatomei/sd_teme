package com.sd.laborator.microservices

import com.sd.laborator.controllers.RabbitMqController
import com.sd.laborator.interfaces.LibraryDAO
import com.sd.laborator.interfaces.LibraryPrinter
import com.sd.laborator.model.Book
import org.springframework.amqp.core.AmqpTemplate
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Controller
class LibraryPrinterMicroservice {
    @Autowired
    private lateinit var libraryDAO: LibraryDAO

    @Autowired
    private lateinit var libraryPrinter: LibraryPrinter

    @Autowired
    private lateinit var connectionFactory: RabbitMqController

    private lateinit var amqpTemplate: AmqpTemplate

    @Autowired
    fun initTemplate() {
        this.amqpTemplate = connectionFactory.rabbitTemplate()
    }


    @RequestMapping("/print", method = [RequestMethod.GET])
    @ResponseBody
    fun customPrint(@RequestParam(required = true, name = "format", defaultValue = "") format: String): String {

        val response = sendAndReceiveMessage("exists~query=print$format")
        println("Received response:" + response)

        var output: String = ""

        if (response != "") {

            val (timestamp, result) = response.split("~")
            val time = timestamp.split("=")[1]

            if (!hasOneHourPassedSince(time))    //nu a trecut o ora
            {
                output = (result.split("=")[1])
            }
            else {

                output = when (format) {
                    "json" -> libraryPrinter.printJSON(libraryDAO.getBooks())
                    "html" -> libraryPrinter.printHTML(libraryDAO.getBooks())
                    "raw" -> libraryPrinter.printRaw(libraryDAO.getBooks())
                    else -> "Not implemented"
                }
                sendAndReceiveMessage("update~query=print$format;result=$output")
            }
        } else {
            output = when (format) {
                "json" -> libraryPrinter.printJSON(libraryDAO.getBooks())
                "html" -> libraryPrinter.printHTML(libraryDAO.getBooks())
                "raw" -> libraryPrinter.printRaw(libraryDAO.getBooks())
                else -> "Not implemented"
            }
            sendAndReceiveMessage("add~query=print$format;result=$output")

        }
        return output

    }

    @RequestMapping("/find", method = [RequestMethod.GET])
    @ResponseBody
    fun customFind(
        @RequestParam(required = false, name = "author", defaultValue = "") author: String,
        @RequestParam(required = false, name = "title", defaultValue = "") title: String,
        @RequestParam(required = false, name = "publisher", defaultValue = "") publisher: String
    ): String {
        var query = ""
        var findBy=""

        if (author != "") {
            query = "Autor${author.replace(" ", "", false)}"
            findBy="author"
        }
        if (title != "") {
            query = "Title${title.replace(" ", "", false)}"
            findBy="title"
        }
        if (publisher != "") {
            query = "Publisher${publisher.replace(" ", "", false)}"
            findBy="publisher"
        }

        val response = sendAndReceiveMessage("exists~query=find$query")

        var output = ""

        if (response != "") {

            val (timestamp, result) = response.split("~")
            val time = timestamp.split("=")[1]

            if (!hasOneHourPassedSince(time))    //nu a trecut o ora
            {
                output = (result.split("=")[1])
            } else {
                output = when(findBy) {
                    "author"->this.libraryPrinter.printJSON(this.libraryDAO.findAllByAuthor(author))
                    "title"->this.libraryPrinter.printJSON(this.libraryDAO.findAllByTitle(title))
                    "publisher"->this.libraryPrinter.printJSON(this.libraryDAO.findAllByPublisher(publisher))
                    else -> ""
                }
                sendAndReceiveMessage("update~query=find$query;result=$output")

            }
        } else {
            output = when(findBy) {
                "author"->this.libraryPrinter.printJSON(this.libraryDAO.findAllByAuthor(author))
                "title"->this.libraryPrinter.printJSON(this.libraryDAO.findAllByTitle(title))
                "publisher"->this.libraryPrinter.printJSON(this.libraryDAO.findAllByPublisher(publisher))
                else -> ""
            }
            sendAndReceiveMessage("add~query=find$query;result=$output")

        }
        return output
    }



    @RequestMapping("/add", method = [RequestMethod.POST])
    @ResponseBody
    fun addBook(@RequestBody book: Book) {
        libraryDAO.addBook(book)

    }


    fun sendAndReceiveMessage(msg: String): String {

        val response = this.amqpTemplate.convertSendAndReceive(
            connectionFactory.getExchange(),
            connectionFactory.getRoutingKey(),
            msg
        )
        return response.toString()
    }

    fun hasOneHourPassedSince(time: String): Boolean {
        val current_date = LocalDateTime.now().minusHours(1)
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")
        val cache_date = LocalDateTime.parse(time, formatter)

        return cache_date.isBefore(current_date)

    }

}