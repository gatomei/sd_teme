package com.sd.laborator.cache.microservices

import com.sd.laborator.cache.controller.RabbitMqController
import com.sd.laborator.cache.interfaces.CachingDAO
import org.springframework.amqp.core.AmqpTemplate
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller

@Controller
class CachingMicroservice {
    @Autowired
    private lateinit var cachingDAO:CachingDAO

    @Autowired
    private lateinit var connectionFactory: RabbitMqController

    private lateinit var amqpTemplate: AmqpTemplate

    @Autowired
    fun initTemplate() {
        this.amqpTemplate = connectionFactory.rabbitTemplate()
    }

    @RabbitListener(queues = ["\${libraryapp.rabbitmq.queue}"])
    fun fetchMessage(message: String):String {
//        println("Message:")
//        println(message)

        try {

            val (operation, parameters) = message.split('~')

            var query: String? = null
            var result: String? = null

            val params: List<String> = parameters.split(';')
            query = params[0].split("=")[1]
            if ("result" in parameters)
                result = params[1].split("=")[1]

            var output: Any? = when (operation) {

                "add" -> cachingDAO.addToCache(query!!, result!!)
                "exists"->cachingDAO.exists(query!!)
                "update"->cachingDAO.updateCache(query!!, result!!)
                else -> null
            }
            if (output != null)
                return output.toString()
        }
        catch( ex:Exception)
        {
            println(ex.toString())
        }
        return ""
    }


}