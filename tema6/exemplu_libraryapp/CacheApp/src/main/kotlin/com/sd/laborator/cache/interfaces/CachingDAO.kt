package com.sd.laborator.cache.interfaces

interface CachingDAO {
    fun exists(query:String):String?
    fun addToCache(query:String, result: String)
    fun updateCache(query: String, result: String)
}