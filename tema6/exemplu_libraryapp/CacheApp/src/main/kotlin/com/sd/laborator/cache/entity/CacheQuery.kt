package com.sd.laborator.cache.entity

data class CacheQuery( private var timestamp:String?, private var result:String?)
{
    var Timestamp: String?
        get() {
            return timestamp
        }
        set(value) {
            timestamp = value
        }

    var Result: String?
        get() {
            return result
        }
        set(value) {
            result = value
        }

    override fun toString(): String {
        return "timestamp="+timestamp+"~result="+result
    }

}