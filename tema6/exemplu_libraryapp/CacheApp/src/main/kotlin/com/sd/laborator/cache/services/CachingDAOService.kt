package com.sd.laborator.cache.services

import com.sd.laborator.cache.entity.CacheQuery
import com.sd.laborator.cache.interfaces.CachingDAO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Service
import java.sql.ResultSet
import java.sql.SQLException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


class CacheRowMapper : RowMapper<CacheQuery?> {
    @Throws(SQLException::class)
    override fun mapRow(rs: ResultSet, rowNum: Int): CacheQuery? {
        return CacheQuery(rs.getString("timestamp"), rs.getString("result"))
    }
}

@Service
class CachingDAOService:CachingDAO {
    @Autowired
    private lateinit var jdbcTemplate: JdbcTemplate

    override fun exists(query: String): String? {

        val result: MutableList<CacheQuery?> = jdbcTemplate.query("SELECT * FROM Cache c where c.query like '"+query+"';", CacheRowMapper())
        if (result.size>0)
            return result[0].toString()
        else
            return null
    }

    override fun addToCache(query: String, result: String) {
        val date = LocalDateTime.now()
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")
        val formatted = date.format(formatter)

        jdbcTemplate.update("INSERT INTO Cache(timestamp, query, result) " +
                "VALUES (?, ?, ?)",formatted , query, result)
    }

    override fun updateCache(query: String, result: String) {
        val date = LocalDateTime.now()
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")
        val formatted = date.format(formatter)

        jdbcTemplate.update("UPDATE Cache SET timestamp='$formatted', result='$result' where query='$query';")
    }
}