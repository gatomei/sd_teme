package com.sd.laborator

import javafx.application.Application.launch
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.lang.Exception
import java.net.ServerSocket
import java.net.Socket

class HeartbeatMicroservice {
    private val subscribers: HashMap<Int, Socket>
    private lateinit var heartbeatSocket: ServerSocket
    private var subscribersTime: MutableMap<Int, Int>

    companion object Constants {
        const val HEARTBEAT_PORT = 1900
        const val MAX_TIME = 5
    }

    init {
        subscribers = hashMapOf()
        subscribersTime = mutableMapOf()
    }

    val message = "RUStillAlive?"
    var isAlive = true

    private fun broadcastMessage() {
        subscribers.forEach {

            it.value.getOutputStream()?.write((message + "\n").toByteArray())
            subscribersTime[it.key] = subscribersTime[it.key]!!.minus(1)
            if (subscribersTime[it.key]!! < 0) {
                isAlive = false
                println("Microserviciul ${it.key} nu mai e activ")
            }
         // println(subscribersTime[it.key])
        }

    }


    public fun run() = runBlocking<Unit> {
        // se porneste un socket server TCP pe portul 1900 care asculta pentru conexiuni
        heartbeatSocket = ServerSocket(HEARTBEAT_PORT)
        println("HeartbeatMicroservice se executa pe portul: ${heartbeatSocket.localPort}")
        println("Se asteapta conexiuni si mesaje...")

        launch(Dispatchers.IO) {
            while (isAlive) {
                broadcastMessage()
                delay(1000)
            }

        }

        while (isAlive) {
            // se asteapta conexiuni din partea clientilor subscriberi
            val clientConnection = heartbeatSocket.accept()
            broadcastMessage()

            // se porneste un thread separat pentru tratarea conexiunii cu clientul
            launch(Dispatchers.IO) {
                println("Subscriber conectat: ${clientConnection.inetAddress.hostAddress}:${clientConnection.port}")

                // adaugarea in lista de subscriberi trebuie sa fie atomica!
                synchronized(subscribers) {
                    subscribers[clientConnection.port] = clientConnection
                    subscribersTime[clientConnection.port] = MAX_TIME
                }

                val bufferReader = BufferedReader(InputStreamReader(clientConnection.inputStream))

                while (true) {
                    // se citeste raspunsul de pe socketul TCP
                    val receivedMessage = bufferReader.readLine()

                    // daca se primeste un mesaj gol (NULL), atunci inseamna ca cealalta parte a socket-ului a fost inchisa
                    if (receivedMessage == null) {
                        // deci subscriber-ul respectiv a fost deconectat
                        println("Subscriber-ul ${clientConnection.port} a fost deconectat.")
                        synchronized(isAlive) {
                            isAlive = false
                        }
                        synchronized(subscribers) {
                            subscribers.remove(clientConnection.port)
                            subscribersTime.remove(clientConnection.port)
                        }
                        bufferReader.close()
                        clientConnection.close()
                        break
                    }

                    println("Primit mesaj: $receivedMessage de la ${clientConnection.port}")
                    synchronized(subscribersTime) {
                        subscribersTime[clientConnection.port] = subscribersTime[clientConnection.port]!!.plus(1)
                    }

                }

            }

        }
    }
}


fun main(args: Array<String>) {
    val heartbeatMicroservice = HeartbeatMicroservice()
    heartbeatMicroservice.run()
}
