#!/usr/bin/env python
"""mapper.py"""

import sys
import urllib.request
from html.parser import HTMLParser

anchors = ""


class MyHTMLParser(HTMLParser):

    def handle_starttag(self, tag, attrs):
        global anchors
        # Only parse the 'anchor' tag.
        if tag == "a":
            # Check the list of defined attributes.
            for name, value in attrs:
                # If href is defined, print it.
                if name == "href":
                    anchors += value + "\n"


# input comes from STDIN (standard input)


for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()

    fp = urllib.request.urlopen(line)
    mybytes = fp.read()

    mystr = mybytes.decode("utf8")
    fp.close()
    parser = MyHTMLParser()
    parser.feed(mystr)

    anchors = anchors.split("\n")

    for anchor in anchors:

        # write the results to STDOUT (standard output);
        # what we output here will be the input for the
        # Reduce step, i.e. the input for reducer.py
        if anchor:
            print('%s\t%s' % (line, anchor))

    anchors = ""
