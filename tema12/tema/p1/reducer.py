#!/usr/bin/env python
"""reducer.py"""

import sys

page2urls = {}

# input comes from STDIN
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()

    # parse the input we got from mapper.py
    page_url, intern_url = line.split()

    # this IF-switch only works because Hadoop sorts map output
    # by key (here: word) before it is passed to the reducer
    if page_url in page2urls.keys():
        if intern_url not in page2urls[page_url]:
            page2urls[page_url].append(intern_url)
    else:
        page2urls[page_url] = []
        page2urls[page_url].append(intern_url)


for k, v in page2urls.items():
    print(k,":", v, "\n")