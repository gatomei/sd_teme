#!/usr/bin/env python
"""reducer.py"""

import sys

letter = None
word = None

dictionar = {}

# input comes from STDIN
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()

    # parse the input we got from mapper.py
    letter, word = line.split()
    if letter in dictionar.keys():
        if word not in dictionar[letter]:
            dictionar[letter].append(word)
    else:
        dictionar[letter]=[]
        dictionar[letter].append(word)

for (k,v) in dictionar.items():
    print('%s\t%s\n' % (k, v))
