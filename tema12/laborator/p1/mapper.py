#!/usr/bin/env python
"""mapper.py"""

import sys
import re
import string

# input comes from STDIN (standard input)
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()
    # split the line into words
    words = line.split()



    for word in words:
        # write the results to STDOUT (standard output);
        # what we output here will be the input for the
        # Reduce step, i.e. the input for reducer.py
        #
        word = re.sub('['+string.punctuation+']', '', word)     #eliminare semne de punctuatie din cuvinte
        if word and word[0].isalpha():
            print('%s\t%s' % (word[0], word))
