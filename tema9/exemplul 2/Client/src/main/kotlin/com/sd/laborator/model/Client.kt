package com.sd.laborator.model

class Client(
    private var nume:String = "",
    private var prenume:String="",
    private var adresaLivrare:String="",
    private var adresaFacturare:String="",
    private var telefon:String=""
)
{

    var Nume:String
        get(){
            return nume
        }
        set(value){
            nume=value
        }

    var Prenume:String
        get(){
            return prenume
        }
        set(value){
            prenume=value
        }

    var AdresaLivrare:String
        get(){
            return adresaLivrare
        }
        set(value){
            adresaLivrare=value
        }

    var AdresaFacturare:String
        get(){
            return adresaFacturare
        }
        set(value){
            adresaFacturare=value
        }

    var Telefon:String
        get(){
            return telefon
        }
        set(value){
            telefon=value
        }



}