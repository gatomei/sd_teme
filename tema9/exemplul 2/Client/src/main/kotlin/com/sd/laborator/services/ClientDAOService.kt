package com.sd.laborator.services

import com.sd.laborator.model.Client
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.sql.ResultSet
import java.sql.SQLException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper


@Service
class ClientDAOService
{

    @Autowired
    private lateinit var jdbcTemplate: JdbcTemplate

    fun inregistreaza(client:Client):Long?
    {
        jdbcTemplate.update(
            "INSERT INTO Client(nume, prenume, adresaLivrare, adresaFacturare, telefon) " +
                    "VALUES(?, ?, ?, ?, ?)",
            client.Nume,
            client.Prenume,
            client.AdresaLivrare,
            client.AdresaFacturare,
            client.Telefon
        )
        val rowMapper:RowMapper<Long> = RowMapper<Long>{resultSet: ResultSet, rowIndex: Int -> resultSet.getLong("ID")}

        val id:Long? = jdbcTemplate.queryForObject("SELECT c.ID FROM Client c WHERE c.nume = '${client.Nume}'", rowMapper)
        return id!!
}

    fun productExists(productName:String):Boolean
    {
        val rowMapper:RowMapper<Long> = RowMapper<Long>{resultSet: ResultSet, rowIndex: Int -> resultSet.getLong("ID")}
        val id:Long? = jdbcTemplate.queryForObject("SELECT p.ID FROM Produs p WHERE p.denumire = '$productName'", rowMapper)
        if (id!=null)
            return true
        return false
    }

}