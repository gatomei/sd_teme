package com.sd.laborator

import com.sd.laborator.models.Comanda
import com.sd.laborator.services.ComandaDAOService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.stream.annotation.EnableBinding
import org.springframework.cloud.stream.messaging.Processor
import org.springframework.integration.annotation.Transformer
import org.springframework.messaging.support.MessageBuilder
import java.io.File
import java.text.DateFormat
import java.text.SimpleDateFormat
import kotlin.random.Random

@EnableBinding(Processor::class)
@SpringBootApplication
open class ComandaMicroservice {

    @Autowired
    private lateinit var comandaDAOService: ComandaDAOService


    private fun pregatireComanda(idClient: Long, produs: String, cantitate: Int): Long {
        println("Se pregateste comanda $cantitate x \"$produs\"...")

        ///TODO - asignare numar de inregistrare
        val idProdus= comandaDAOService.getProductId(produs)
        val comanda = Comanda(idClient, idProdus,cantitate)
        val nrInregistrare =comandaDAOService.adaugaComanda(comanda)

        ///TODO - inregistrare comanda in baza de date

        return nrInregistrare
    }

    @Transformer(inputChannel = Processor.INPUT, outputChannel = Processor.OUTPUT)
    fun preluareComanda(comanda: String?): String {
        val (identitateClient, produsComandat, cantitate) = comanda!!.split("|")
        println("Am primit comanda urmatoare: ")
        println("$identitateClient | $produsComandat | $cantitate ")

        val idComanda = pregatireComanda(identitateClient.toLong(),produsComandat, cantitate.toInt())

        ///TODO - in loc sa se trimita mesajul cu toate datele in continuare, trebuie trimis doar ID-ul comenzii
        return "$idComanda"
    }
}

fun main(args: Array<String>) {
    runApplication<ComandaMicroservice>(*args)
}