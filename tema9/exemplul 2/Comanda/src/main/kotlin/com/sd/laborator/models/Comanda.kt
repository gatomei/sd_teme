package com.sd.laborator.models

class Comanda(private var clientId:Long, private var productId:Long, private var cantitate:Int) {

    var ClientID:Long
        get() { return clientId}
        set(value) {clientId=value}

    var ProductId:Long
        get() {return productId}
        set(value) {productId =value}

    var Cantitate:Int
        get() { return cantitate}
        set(value) { cantitate = value}

}