package com.sd.laborator.services

import com.sd.laborator.models.Comanda
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Service
import java.sql.ResultSet


@Service
class ComandaDAOService {

    @Autowired
    private lateinit var jdbcTemplate: JdbcTemplate

    fun getProductId(productName:String):Long
    {
        val rowMapper: RowMapper<Long> = RowMapper<Long>{ resultSet: ResultSet, rowIndex: Int -> resultSet.getLong("ID")}
        val id:Long? = jdbcTemplate.queryForObject("SELECT p.ID FROM Produs p WHERE p.denumire = '$productName'", rowMapper)
        return id!!
    }

    fun adaugaComanda(comanda:Comanda):Long
    {
        jdbcTemplate.update(
            "INSERT INTO Comanda(ID_client, ID_produs, cantitate) " +
                    "VALUES(?, ?, ?)",
            comanda.ClientID,
            comanda.ProductId,
            comanda.Cantitate)

        val rowMapper:RowMapper<Long> = RowMapper<Long>{resultSet: ResultSet, rowIndex: Int -> resultSet.getLong("ID")}

        val id:Long? = jdbcTemplate.queryForObject("SELECT c.ID FROM Comanda c WHERE c.ID_client = '${comanda.ClientID}'", rowMapper)
        return id!!

    }

}