package com.sd.laborator.services

import com.sd.laborator.components.RabbitMqComponent
import org.springframework.amqp.core.AmqpTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Service
import java.sql.ResultSet
import java.sql.SQLException


class Comanda(val idClient:Long, val productId:Long, val cantitate:Int )

@Service
class DepozitDAOService {

    @Autowired
    private lateinit var jdbcTemplate: JdbcTemplate

    @Autowired
    private lateinit var rabbitMqComponent: RabbitMqComponent
    private lateinit var amqpTemplate: AmqpTemplate
    @Autowired
    fun initTemplate() {
        this.amqpTemplate = rabbitMqComponent.rabbitTemplate()
    }


    fun getStoc(idProdus:Long):Int
    {
        val rowMapper: RowMapper<Int> = RowMapper<Int>{ resultSet: ResultSet, _ -> resultSet.getInt("stoc_disponibil")}
        val stoc:Int? = jdbcTemplate.queryForObject("SELECT p.stoc_disponibil FROM Produs p WHERE p.ID = '$idProdus'", rowMapper)

        return when(stoc)
        {
            null-> 0
            else ->stoc
        }


    }

    fun getDetaliiComanda(idComanda:Long):Comanda
    {
        class ComandaRowMapper : RowMapper<Comanda?> {
            @Throws(SQLException::class)
            override fun mapRow(rs: ResultSet, rowNum: Int): Comanda {
                return Comanda(rs.getLong("ID_client"), rs.getLong("ID_produs"), rs.getInt("cantitate"))
            }
        }

        val result: Comanda? = jdbcTemplate.queryForObject("SELECT c.ID_client, c.ID_produs, c.cantitate FROM Comanda c WHERE c.ID = '$idComanda'", ComandaRowMapper())
        return result!!
    }

    fun retrageProdus(idProdus:Long, cantitate: Int)
    {
        jdbcTemplate.execute("UPDATE Produs SET stoc_disponibil=stoc_disponibil-$cantitate WHERE ID=$idProdus")

    }



    fun shouldCommandProduct(idProdus: Long)
    {
        val rowMapper: RowMapper<Int> = RowMapper<Int>{ resultSet: ResultSet, _ -> resultSet.getInt("number")}

        val no:Int = jdbcTemplate.queryForObject("SELECT COUNT(*) as number FROM Comanda WHERE ID_produs==$idProdus", rowMapper)

        if(no%3==0)
            sendRestockMessage(idProdus)

    }


 fun sendRestockMessage(idProdus: Long) {
        println("message: ")

        this.amqpTemplate.convertAndSend(rabbitMqComponent.getExchange(),
            rabbitMqComponent.getRoutingKey(), "$idProdus")
    }

}