# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Comanda.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets

import qdarkstyle
import socket
import threading



HOST = "localhost"
CLIENT_PORT = 1600

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("Comanda")
        MainWindow.resize(891, 675)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.comanda_bttn = QtWidgets.QPushButton(self.centralwidget)
        self.comanda_bttn.setGeometry(QtCore.QRect(720, 560, 151, 41))
        self.comanda_bttn.setAutoFillBackground(False)
        self.comanda_bttn.setStyleSheet("background-color: rgb(255, 85, 0);")
        self.comanda_bttn.setObjectName("comanda_bttn")
        self.numeText = QtWidgets.QLineEdit(self.centralwidget)
        self.numeText.setGeometry(QtCore.QRect(140, 160, 191, 31))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.numeText.setFont(font)
        self.numeText.setObjectName("numeText")
        self.welcomeLabel = QtWidgets.QLabel(self.centralwidget)
        self.welcomeLabel.setGeometry(QtCore.QRect(300, 30, 230, 51))
        font = QtGui.QFont()
        font.setPointSize(22)
        font.setBold(True)
        font.setWeight(75)
        self.welcomeLabel.setFont(font)
        self.welcomeLabel.setStyleSheet("color: rgb(255, 85, 0);")
        self.welcomeLabel.setObjectName("welcomeLabel")
        self.numeLabel = QtWidgets.QLabel(self.centralwidget)
        self.numeLabel.setGeometry(QtCore.QRect(40, 160, 81, 31))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.numeLabel.setFont(font)
        self.numeLabel.setObjectName("numeLabel")
        self.prenumeLabel = QtWidgets.QLabel(self.centralwidget)
        self.prenumeLabel.setGeometry(QtCore.QRect(40, 219, 101, 31))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.prenumeLabel.setFont(font)
        self.prenumeLabel.setObjectName("prenumeLabel")
        self.prenumeText = QtWidgets.QLineEdit(self.centralwidget)
        self.prenumeText.setGeometry(QtCore.QRect(140, 220, 191, 31))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.prenumeText.setFont(font)
        self.prenumeText.setObjectName("prenumeText")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(40, 290, 170, 31))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.adresaLivrareText = QtWidgets.QTextEdit(self.centralwidget)
        self.adresaLivrareText.setGeometry(QtCore.QRect(200, 290, 281, 87))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.adresaLivrareText.setFont(font)
        self.adresaLivrareText.setObjectName("adresaLivrareText")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(40, 410, 170, 31))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.adresaFacturareText = QtWidgets.QTextEdit(self.centralwidget)
        self.adresaFacturareText.setGeometry(QtCore.QRect(210, 410, 271, 87))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.adresaFacturareText.setFont(font)
        self.adresaFacturareText.setObjectName("adresaFacturareText")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(40, 530, 151, 31))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.telefonText = QtWidgets.QLineEdit(self.centralwidget)
        self.telefonText.setGeometry(QtCore.QRect(200, 530, 221, 31))
        self.telefonText.setObjectName("telefonText")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(500, 150, 121, 31))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.label_4.setFont(font)
        self.label_4.setObjectName("label_4")
        self.produsText = QtWidgets.QLineEdit(self.centralwidget)
        self.produsText.setGeometry(QtCore.QRect(650, 150, 221, 31))
        self.produsText.setObjectName("produsText")
        self.cantitate = QtWidgets.QLineEdit(self.centralwidget)
        self.cantitate.setGeometry(QtCore.QRect(650, 210, 211, 31))
        self.cantitate.setObjectName("cantitate")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(500, 210, 111, 21))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.label_5.setFont(font)
        self.label_5.setObjectName("label_5")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 891, 26))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Comanda"))
        self.comanda_bttn.setText(_translate("MainWindow", "Plaseaza comanda"))
        self.welcomeLabel.setText(_translate("MainWindow", "WELCOME!"))
        self.numeLabel.setText(_translate("MainWindow", "Nume:"))
        self.prenumeLabel.setText(_translate("MainWindow", "Prenume:"))
        self.label.setText(_translate("MainWindow", "Adresa de livrare:"))
        #self.adresaLivrareText.setText(_translate("MainWindow", ":"))
        self.label_2.setText(_translate("MainWindow", "Adresa de facturare:"))
        self.label_3.setText(_translate("MainWindow", "Numar de telefon:"))
        self.label_4.setText(_translate("MainWindow", "Nume produs:"))
        self.label_5.setText(_translate("MainWindow", "Cantitate:"))
        self.comanda_bttn.clicked.connect(self.comanda)


    def send_message(self, message):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.sock.connect((HOST, CLIENT_PORT))
            print(message)
            self.sock.send(bytes(message+"\n", "utf-8"))

        except Exception as e:
            print(e)

    def comanda(self):
        if not self.cantitate.text().isnumeric():
            return
        message_comp = [self.numeText.text(), self.prenumeText.text(), self.adresaLivrareText.toPlainText(),
                       self.adresaFacturareText.toPlainText(), self.telefonText.text(), self.produsText.text(), self.cantitate.text()]
        message = ",".join(message_comp)
        threading.Thread(target=self.send_message, args=(message,)).start()


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    stylesheet = qdarkstyle.load_stylesheet_pyqt5()
    app.setStyleSheet(stylesheet)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
