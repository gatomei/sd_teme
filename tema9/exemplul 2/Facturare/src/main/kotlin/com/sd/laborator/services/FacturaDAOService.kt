package com.sd.laborator.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Service
import java.sql.ResultSet
import java.time.LocalDateTime

@Service
class FacturaDAOService {

    @Autowired
    private lateinit var jdbcTemplate: JdbcTemplate

    fun inregistrareFactura(idComanda:Long):Long
    {
        jdbcTemplate.update("INSERT INTO Factura(ID_comanda, data) " +
                    "VALUES(?, ?)",idComanda, LocalDateTime.now())

        val rowMapper: RowMapper<Long> = RowMapper<Long>{ resultSet: ResultSet, _ -> resultSet.getLong("nr_inregistrare")}

        val id:Long? = jdbcTemplate.queryForObject("SELECT nr_inregistrare FROM Factura WHERE ID_comanda = '${idComanda}'", rowMapper)
        return id!!

    }
}